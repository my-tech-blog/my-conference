# MyConference

Blog Exercise - CQRS + ES !!


### Article URL


### Domain

We will develop an API to create a conference and sell the tickets for the conference. To create the conference, the conference creator has to provide the number of seats available to be booked. It is not allowed to create the conference without the number of available seats. Once the conference is created, a user can buy a ticket to obtain one or more seats. We cannot overbook the conference. That's it; Quite a simple domain.


### Tools used

1. Java 8
2. Spring Boot 2
3. H2 - In memory embedded database
4. JPA
5. Axon Server + Framework
6. Tomcat - Embedded Web Server

### How to run the app

1. Run the axon server

> Bash
```bash
java -jar axonserver-4.0.jar
```
2. Run the application

> Maven
```bash
mvn spring-boot:run
```

### Links

* [H2 Database Console](http://localhost:8080/console/)
* [Axon Server](http://localhost:8024)

