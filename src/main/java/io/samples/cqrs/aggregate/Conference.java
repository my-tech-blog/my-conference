package io.samples.cqrs.aggregate;

import io.samples.cqrs.command.*;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.modelling.command.AggregateMember;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.ArrayList;
import java.util.List;

@Aggregate
public class Conference {

    @AggregateIdentifier
    private String conferenceId;

    private String conferenceName;

    private int availableSeats;

    @AggregateMember
    private List<Ticket> tickets;


    public Conference() {
        // Required for Axon Framework
    }

    @CommandHandler
    public Conference(CreateConferenceCommand command) {
        if(command.getAvailableSeats() <= 0) {
            throw new InvalidConferenceData("Available seats must be greater than 0");
        }
        AggregateLifecycle.apply(new ConferenceCreatedEvent(command.getConferenceId(), command.getConferenceName(), command.getAvailableSeats()));
    }

    @CommandHandler
    public void sellTicket(SellTicketCommand command) {
        if(command.getNoOfTickets() > this.availableSeats) {
            throw new InvalidConferenceData("It is not possible to get more tickets than available.");
        }
        AggregateLifecycle.apply(new TicketSoldEvent(command.getTicketId(), command.getConferenceId(), command.getNoOfTickets(), this.availableSeats -  command.getNoOfTickets()));
    }

    @EventSourcingHandler
    public void handle(ConferenceCreatedEvent event) {
        this.conferenceId = event.getConferenceId();
        this.conferenceName = event.getConferenceName();
        this.availableSeats = event.getAvailableSeats();
        this.tickets = new ArrayList<Ticket>();
    }

    @EventSourcingHandler
    public void handle(TicketSoldEvent event) {
        tickets.add(new Ticket(event.getTicketId(), event.getConferenceId(), event.getNoOfTickets()));
        this.availableSeats = event.getAvailableSeats();
    }

}
