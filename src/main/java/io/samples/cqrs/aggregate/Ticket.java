package io.samples.cqrs.aggregate;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.axonframework.modelling.command.EntityId;

@Data
@AllArgsConstructor
public class Ticket {
    @EntityId
    private String ticketId;
    private String conferenceId;
    private int noOfTickets;
}
