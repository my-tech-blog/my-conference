package io.samples.cqrs.api;

import io.samples.cqrs.command.CreateConferenceCommand;
import io.samples.cqrs.command.SellTicketCommand;
import io.samples.cqrs.query.*;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class ConferenceEndpoint {

    @Autowired
    private CommandGateway commandGateway;

    @Autowired
    private QueryGateway queryGateway;

    @GetMapping("conferences/{conferenceId}")
    public ConferenceProjection getConference(@PathVariable String conferenceId) {
        return queryGateway.query(new ConferenceQuery(conferenceId), ConferenceProjection.class).join();
    }

    @GetMapping("conferences/{conferenceId}/ticket")
    public List<TicketProjection> getConferenceTickets(@PathVariable String conferenceId) {
        return queryGateway.query(new TicketQuery(conferenceId), TicketQueryResponse.class).join().getTickets();
    }

    @PostMapping("conference")
    public void createConference(@RequestParam String name) {
        String conferenceId = UUID.randomUUID().toString();
        commandGateway.send(new CreateConferenceCommand(conferenceId, name, 50));
    }

    @PostMapping("ticket")
    public void sellTicket(@RequestParam String conferenceId, @RequestParam int noOftickets) {
        String ticketId = UUID.randomUUID().toString();
        commandGateway.send(new SellTicketCommand(ticketId, conferenceId, noOftickets));
    }
}
