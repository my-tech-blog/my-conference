package io.samples.cqrs.command

import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.lang.RuntimeException

data class CreateConferenceCommand (
        @TargetAggregateIdentifier val conferenceId: String,
        val conferenceName: String,
        val availableSeats: Int
)

data class ConferenceCreatedEvent (
        @TargetAggregateIdentifier val conferenceId: String,
        val conferenceName: String,
        val availableSeats: Int
)

data class SellTicketCommand (
        val ticketId: String,
        @TargetAggregateIdentifier val conferenceId: String,
        val noOfTickets: Int
)

data class TicketSoldEvent (
        val ticketId: String,
        val conferenceId: String,
        val noOfTickets: Int,
        val availableSeats: Int? = 0
)

class InvalidConferenceData(message: String): RuntimeException(message)
