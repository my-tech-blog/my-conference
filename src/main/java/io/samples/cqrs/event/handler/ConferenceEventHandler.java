package io.samples.cqrs.event.handler;


import io.samples.cqrs.command.ConferenceCreatedEvent;
import io.samples.cqrs.command.TicketSoldEvent;
import io.samples.cqrs.query.*;
import io.samples.cqrs.repository.ConferenceProjectionRepository;
import io.samples.cqrs.repository.TicketProjectionRepository;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ConferenceEventHandler {

    @Autowired
    private ConferenceProjectionRepository conferenceRepository;

    @Autowired
    private TicketProjectionRepository ticketRepository;

    @EventHandler
    public void on(ConferenceCreatedEvent event) {
        conferenceRepository.save(new ConferenceProjection(event.getConferenceId(), event.getConferenceName(), event.getAvailableSeats()));
    }

    @EventHandler
    public void on(TicketSoldEvent event) {
        Optional<ConferenceProjection> optionalConference = conferenceRepository.findById(event.getConferenceId());
        ConferenceProjection conferenceProjection = optionalConference.orElseThrow(() -> new ConferenceNotFoundException("Conference Not Found"));
        conferenceProjection.setAvailableSeats(event.getAvailableSeats());
        conferenceRepository.save(conferenceProjection);
        ticketRepository.save(new TicketProjection(event.getTicketId(), event.getConferenceId(), event.getNoOfTickets()));
    }

    @QueryHandler
    public ConferenceProjection getConference(ConferenceQuery query) {
        return getConference(query.getConferenceId());
    }

    @NotNull
    private ConferenceProjection getConference(String conferenceId) {
        Optional<ConferenceProjection> optionalConference = conferenceRepository.findById(conferenceId);

        return optionalConference.orElseThrow(() -> new ConferenceNotFoundException("Conference Not Found"));
    }

    @QueryHandler
    public TicketQueryResponse getTickets(TicketQuery query) {

        ConferenceProjection conference = getConference(query.getConferenceId());

        TicketProjection ticketProjection = new TicketProjection();
        ticketProjection.setConferenceId(conference.getConferenceId());

        List<TicketProjection> tickets = ticketRepository.findAll(Example.of(ticketProjection, ExampleMatcher.matchingAny()));
        return new TicketQueryResponse(tickets);
    }


}
