package io.samples.cqrs.query

import java.lang.RuntimeException
import javax.persistence.Entity
import javax.persistence.Id

data class ConferenceQuery(
        var conferenceId: String? = null
)

data class TicketQuery(
        var conferenceId: String? = null
)

data class TicketQueryResponse (
        var tickets: List<TicketProjection>? = null
)

@Entity
data class ConferenceProjection (
        @Id var conferenceId: String? = null,
        var conferenceName: String? = null,
        var availableSeats: Int? = 0
)

@Entity
data class TicketProjection (
        @Id var ticketId: String? = null,
        var conferenceId: String? = null,
        var noOfTickets: Int? = 0
)

class ConferenceNotFoundException(message: String): RuntimeException(message)
class TicketNotFoundException(message: String): RuntimeException(message)