package io.samples.cqrs.repository;

import io.samples.cqrs.query.ConferenceProjection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConferenceProjectionRepository extends JpaRepository<ConferenceProjection, String> {
}
