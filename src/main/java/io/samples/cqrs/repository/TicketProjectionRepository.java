package io.samples.cqrs.repository;

import io.samples.cqrs.query.TicketProjection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TicketProjectionRepository extends JpaRepository<TicketProjection, String> {
}
