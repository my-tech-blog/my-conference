package io.samples.cqrs.command;

import io.samples.cqrs.aggregate.Conference;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;

public class ConferenceTest {
    FixtureConfiguration<Conference> target;


    @Before
    public void setup() {
        target = new AggregateTestFixture<Conference>(Conference.class);
    }

    @Test
    public void testCreateConference() {
        target.givenNoPriorActivity()
                .when(new CreateConferenceCommand("C001", "MyConference", 50))
                .expectEvents(new ConferenceCreatedEvent("C001", "MyConference",50));
    }

    @Test
    public void testCreateConferenceWithZeroSeats() {
        target.givenNoPriorActivity()
                .when(new CreateConferenceCommand("C001", "MyConference", 0))
                .expectNoEvents();
    }

    @Test
    public void testSellTicket() {
        target.given(new ConferenceCreatedEvent("C001", "MyConference", 50))
                .when(new SellTicketCommand("T001", "C001", 1))
                .expectEvents(new TicketSoldEvent("T001", "C001", 1, 49));
    }

    @Test
    public void testSellTicketMoreThanAvailable() {
        target.given(new ConferenceCreatedEvent("C001", "MyConference", 50))
                .when(new SellTicketCommand("T001", "C001", 51))
                .expectNoEvents();
    }
}
